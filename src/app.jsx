import React from 'react';
import autobind from 'autobindr';
import '../styles/index.scss';
import Header from './header.jsx';
import FormElement from './form-element.jsx';
import ResultTable from './result-table.jsx'
import ClipboardButton from 'react-clipboard.js';
import Select from 'react-select';
import data from './data.json';
import 'react-select/dist/react-select.css';

export default class App extends React.Component {
  copyBtnTitleText = 'Copy results';
  copiedBtnTitleText = 'Done!';
  constructor(props) {
    super(props);
    autobind(this);
    this.state = {
      /*fullName: 'Brian Merlehan',
      position: 'Chief Marketing Officer',
      mobilePhone: '+38 (097) 266 90 31',
      landlinePhone: '+38 (032) 266 90 31',
      skype: 'brian_merlehan',*/
      fullName: '',
      position: '',
      mobilePhone: '',
      landlinePhone: '',
      skype: '',
      selectedCity: {},
      formFilled: false,
      coppied: false,
      copyBtnTitle: this.copyBtnTitleText
    };
  };

  addChanges(modifiedState){
    this.setState(Object.assign(
      modifiedState,
      {
        formFilled: Boolean(
          modifiedState.fullName &&
          modifiedState.position &&
          modifiedState.mobilePhone
        )
      }
    ))
  }

  handleFullNameChange(event) {
    this.addChanges(Object.assign(this.state, {
      fullName: event.target.value,
      copyBtnTitle: this.copyBtnTitleText
    }));
  };

  handlePositionChange(event) {
    this.addChanges(Object.assign(this.state, {
      position: event.target.value,
      copyBtnTitle: this.copyBtnTitleText
    }));
  };

  handleMobilePhoneChange(event) {
    this.addChanges(Object.assign(this.state, {
      mobilePhone: event.target.value,
      copyBtnTitle: this.copyBtnTitleText
    }));
  };

  handleLandlinePhoneChange(event) {
    this.setState({
      landlinePhone: event.target.value,
      copyBtnTitle: this.copyBtnTitleText
    });
  };

  handleSkypeChange(event) {
    this.setState({
      skype: event.target.value,
      copyBtnTitle: this.copyBtnTitleText
    });
  };

  onSuccess() {
    this.setState({
      coppied: true,
      copyBtnTitle: this.copiedBtnTitleText
    });
  };

  focusStateCitySelect () {
    this.citiesSelectArea.focus();
  };

  updateCityValue (newValue) {
    this.setState({
      selectedCity: newValue,
      copyBtnTitle: this.copyBtnTitleText
    });
  };

  render() {
    return (
      <div>
        <Header/>
        <section className="section">
          <div className="container">
            <div className="row content">
              <div className="form">
                <div className="form__title">To generate your email signature please fill in the fields below</div>
                <FormElement
                  labelText="Full Name"
                  inputName="FullName"
                  inputValue={this.state.fullName}
                  inputOnChange={this.handleFullNameChange}
                  inputIcon='<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><g fill="none" fill-rule="evenodd" stroke-width="2" class="form__icon__stroke"><path d="M11.92 12.465c-2.093 0-3.861-3.01-3.861-6.574 0-3.2 1.335-4.89 3.862-4.89s3.863 1.69 3.863 4.89c0 3.563-1.77 6.574-3.863 6.574M1.7 18.976a6.734 6.734 0 0 1 4.676-5.493l5.568 5.512 5.51-5.57a6.734 6.734 0 0 1 4.732 5.446l.594 4.012-21.632.112.553-4.02z"/></g></svg>'
                  inputRequired={true}
                />
                <FormElement
                  labelText="Position"
                  inputName="Position"
                  inputValue={this.state.position}
                  inputOnChange={this.handlePositionChange}
                  inputIcon='<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><g fill="none" fill-rule="evenodd" stroke-width="2" class="form__icon__stroke"><path d="M12 17.84l-8 4.412V1h16v21.252z"/><path d="M12 11c-1.654 0-3-1.346-3-3s1.346-3 3-3 3 1.346 3 3-1.346 3-3 3"/></g></svg>'
                  inputRequired={true}
                />
                <FormElement
                  labelText="Mobile phone (+380 XX XXX XX XX)"
                  inputName="MobilePhone"
                  inputValue={this.state.mobilePhone}
                  inputOnChange={this.handleMobilePhoneChange}
                  inputIcon='<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><g fill="none" fill-rule="evenodd" stroke-width="2" class="form__icon__stroke"><path d="M5 23h14V1H5z"/><path d="M12 19.5a.5.5 0 1 1 .002-1.002A.5.5 0 0 1 12 19.5"/></g></svg>'
                  inputRequired={true}
                />
                <FormElement
                  labelText="Skype"
                  inputName="Skype"
                  inputValue={this.state.skype}
                  inputOnChange={this.handleSkypeChange}
                  inputIcon='<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><g fill="none" fill-rule="evenodd"><path stroke-width="2" class="form__icon__stroke" d="M3.613 2.117C5.508.749 8.19.63 10.208 1.8c2.276-.394 4.683-.02 6.714 1.087a10.396 10.396 0 0 1 4.875 5.747c.57 1.65.702 3.442.405 5.16a6.2 6.2 0 0 1 .687 4.272 6.168 6.168 0 0 1-2.657 3.93c-1.883 1.266-4.47 1.338-6.433.203-2.183.375-4.488.049-6.465-.96a10.392 10.392 0 0 1-5.025-5.605 10.482 10.482 0 0 1-.503-5.43 6.204 6.204 0 0 1-.67-4.348 6.17 6.17 0 0 1 2.477-3.74"/><path  class="form__icon__fill" d="M10.638 6.081c1.451-.22 2.98.003 4.276.706.566.314 1.114.74 1.377 1.349.172.4.191.924-.125 1.26-.343.351-.903.46-1.364.311-.533-.223-.798-.78-1.25-1.114-.568-.467-1.334-.594-2.05-.58-.523.022-1.084.14-1.465.525-.354.378-.485 1.023-.14 1.447.324.374.826.514 1.286.642.878.22 1.765.396 2.647.604.917.21 1.843.612 2.448 1.359 1.044 1.353.809 3.497-.489 4.604-1.039.919-2.483 1.174-3.828 1.181-1.435-.02-2.97-.314-4.074-1.297-.555-.498-1.02-1.252-.853-2.022.183-.767 1.235-1.057 1.832-.586.455.366.702.923 1.141 1.304.526.482 1.258.674 1.959.657.687.017 1.417-.192 1.888-.715.32-.354.49-.89.281-1.343-.17-.425-.608-.656-1.028-.77-.773-.203-1.552-.388-2.324-.591-.85-.208-1.733-.398-2.464-.905-.57-.378-1.003-.968-1.14-1.642-.235-1.057-.068-2.276.7-3.083.703-.781 1.745-1.145 2.759-1.3"/></g></svg>'
                  inputRequired={false}
                />
                <div
                  className="form__title"
                  style={{
                    marginTop: '50px'
                  }}
                >For the long version please fill in aditional fields:</div>
                <FormElement
                  labelText="Landline phone"
                  inputName="LandlinePhone"
                  inputValue={this.state.landlinePhone}
                  inputOnChange={this.handleLandlinePhoneChange}
                  inputIcon='<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="none" fill-rule="evenodd" stroke-width="2" class="form__icon__stroke" d="M4.877 1c.451 0 .873.173 1.186.486l2.741 2.742c.314.314.487.735.487 1.186 0 .451-.173.872-.487 1.186L6.977 8.428c-2.247 2.246 1.784 6.724 1.827 6.767 1.19 1.189 3.6 2.519 5.042 2.519.668 0 1.28-.246 1.726-.691l1.827-1.828a1.667 1.667 0 0 1 1.186-.488c.45 0 .873.174 1.187.488l2.74 2.74c.314.315.488.737.488 1.188 0 .45-.174.872-.487 1.185l-1.828 1.828a1.65 1.65 0 0 1-.864.456l-.158.03-.15.052c-.623.216-1.332.325-2.107.325-3.419 0-7.623-2.081-10.973-5.432C2.286 13.42.137 7.92 1.326 4.487l.051-.15.031-.158c.062-.33.22-.63.456-.865l1.828-1.828A1.664 1.664 0 0 1 4.877 1"/></svg>'
                  inputRequired={false}
                />
                <div className="form__element">
                  <Select
                    name="form-field-name"
                    className={`customSelect selectCity ${this.state.selectedCity.value ? 'form__control--filled' : ''}`}
                    clearable={false}
                    deleteRemoves={false}
                    options={data.cities}
                    value={this.state.selectedCity}
                    onChange={this.updateCityValue}
                    ref={(citiesSelect) => { this.citiesSelectArea = citiesSelect; }}
                    placeholder=""
                  />
                  <label
                    className="form__label"
                    onClick={this.focusStateCitySelect}
                  >Select Your Office Location</label>
                  <span
                    className="form__icon"
                    dangerouslySetInnerHTML={{__html: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><g fill="none" fill-rule="evenodd" class="form__icon__stroke" stroke-width="2"><path d="M12 12c-1.654 0-3-1.346-3-3s1.346-3 3-3 3 1.346 3 3-1.346 3-3 3"/><path d="M12.25 1A8.25 8.25 0 0 0 4 9.25C4 18.531 12.25 23 12.25 23s8.25-4.469 8.25-13.75A8.25 8.25 0 0 0 12.25 1"/></g></svg>'}}
                  ></span>
                </div>
                <ClipboardButton
                  onSuccess={this.onSuccess}
                  className="btn"
                  data-clipboard-action="copy"
                  data-clipboard-target="#generatedSignature"
                  button-disabled={!this.state.formFilled}
                >{this.state.copyBtnTitle}</ClipboardButton>
                {this.state.coppied && (<div className="form__instruction">
                  <p className="form__instruction__text">
                    <strong>Well done!</strong> Now you can paste it to email.
                  </p>
                  <p className="form__instruction__text">
                    Useful links:
                    <br/>
                    <a
                      className="form__instruction__link"
                      href="//support.google.com/mail/answer/8395?hl=en"
                      target="_blank"
                    >How to set up signature in Gmail?</a>
                    <br/>
                    <a
                      className="form__instruction__link"
                      href="//support.office.com/en-nz/article/Create-and-add-an-e-mail-message-signature-0a81de87-0e98-4fc2-b724-bc2bec7a39ae"
                      target="_blank"
                    >How to set up signature in Microsoft Outlook?</a>
                  </p>
                  </div>)}
              </div>
              <div className="result">
                <img
                  className="result__mask"
                  src="https://eleks.com/files/email-signature/letter.png"
                  alt="letter"
                />
                <ResultTable
                  id="generatedSignatureVisible"
                  fullName={this.state.fullName}
                  position={this.state.position}
                  mobilePhone={this.state.mobilePhone}
                  landlinePhone={this.state.landlinePhone}
                  skype={this.state.skype}
                  selectedCity={this.state.selectedCity}
                />
                <div id="generatedSignatureWrap">
                  <ResultTable
                    id="generatedSignature"
                    fullName={this.state.fullName}
                    position={this.state.position}
                    mobilePhone={this.state.mobilePhone}
                    landlinePhone={this.state.landlinePhone}
                    skype={this.state.skype}
                    selectedCity={this.state.selectedCity}
                  />
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
  }
}
