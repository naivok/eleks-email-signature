import React, {PropTypes} from 'react';

const ResultTable = (props) => (
  <table
    id={props.id}
    cellPadding="0"
    cellSpacing="0"
    width="250"
    style={{backgroundColor: '#fff'}}
  >
    <tbody>
      <tr>
        <td
          colSpan="2"
          style={{
            borderBottom: '1px solid #0060ff'
          }}
        >
          <p style={{
            fontFamily: 'Arial',
            fontSize: '13px',
            lineHeight: '13px',
            marginBottom: '20px',
            marginTop: '0',
          }}>Best regards,</p>
          {props.fullName && (<p style={{
            color: '#000',
            fontFamily: 'Arial',
            fontSize: '16px',
            lineHeight: '16px',
            marginBottom: '5px',
            marginTop: '0',
            wordBreak: 'break-all'
          }}>
            <strong>{props.fullName}</strong>
          </p>)}
          {props.position && (<p style={{
            color: '#0060ff',
            fontFamily: 'Arial',
            fontSize: '13px',
            lineHeight: '13px',
            marginBottom: '10px',
            marginTop: '0',
            wordBreak: 'break-all'
          }}>{props.position}</p>)}
        </td>
      </tr>
      <tr>
        <td style={{
          fontSize: '0',
          lineHeight: '14px',
          width: '200px',
          padding: '8px 10px 0 0',
          verticalAlign: 'top'
        }}>
          <table
            cellPadding="0"
            cellSpacing="0"
            width="100%"
          >
            <tbody>
            {props.mobilePhone && (<tr>
                <td
                  valign="top"
                  style={{
                    padding: '6px 0 2px',
                    fontSize: '0',
                    lineHeight: '14px',
                    textAlign: 'center',
                    verticalAlign: 'top',
                    width: '15px'
                  }}
                >
                  <img
                    src="https://eleks.com/files/email-signature/phone-1.png"
                    alt=""
                  />
                </td>
                <td
                  valign="top"
                  style={{
                    color: '#000',
                    fontFamily: 'Arial',
                    fontSize: '13px',
                    lineHeight: '16px',
                    padding: '4px 0 2px 7px',
                    verticalAlign: 'top',
                    wordBreak: 'break-all'
                  }}
                >
                  {props.mobilePhone}
                </td>
              </tr>)}
          {props.landlinePhone && (<tr>
                <td style={{
                  padding: '6px 0 2px',
                  fontSize: '0',
                  lineHeight: '14px',
                  textAlign: 'center',
                  verticalAlign: 'top',
                  width: '15px'
                }}>
                  <img
                    src="https://eleks.com/files/email-signature/landline-1.png"
                    alt=""
                  />
                </td>
                <td style={{
                  color: '#000',
                  fontFamily: 'Arial',
                  fontSize: '13px',
                  lineHeight: '16px',
                  padding: '4px 0 2px 7px',
                  verticalAlign: 'top',
                  wordBreak: 'break-all'
                }}>
                  {props.landlinePhone}
                </td>
              </tr>)}
          {props.skype && (<tr>
                <td style={{
                  padding: '6px 0 2px',
                  fontSize: '0',
                  lineHeight: '14px',
                  textAlign: 'center',
                  verticalAlign: 'top',
                  width: '15px'
                }}>
                  <img
                    src="https://eleks.com/files/email-signature/skype-1.png"
                    alt=""
                  />
                </td>
                <td style={{
                  color: '#000',
                  fontFamily: 'Arial',
                  fontSize: '13px',
                  lineHeight: '16px',
                  padding: '4px 0 2px 7px',
                  verticalAlign: 'top',
                  wordBreak: 'break-all'
                }}>
                  {props.skype}
                </td>
              </tr>)}
            </tbody>
          </table>
        </td>
        <td style={{
          fontSize: '0',
          lineHeight: '16px',
          padding: '8px 0 5px 0px',
          verticalAlign: 'top',
          textAlign: 'right',
          width: '50px'
        }}>
          <table
              cellPadding="0"
              cellSpacing="0"
              width="100%"
            >
              <tbody>
                <td style={{
                    padding: '4px 0 0',
                    fontSize: '0',
                    lineHeight: '16px',
                    textAlign: 'right',
                    verticalAlign: 'top',
                  }}>
                  <a
                    href="https://eleks.com"
                    target="_blank"
                  >
                    <img
                      src="https://eleks.com/files/email-signature/logo_email.png"
                      alt=""
                      style={{
                        verticalAlign: 'top'
                      }}
                    />
                  </a>
                </td>
              </tbody>
          </table>
        </td>
      </tr>
      {props.selectedCity.value && props.selectedCity.value !== 'none' && ((<tr>
        <td colSpan="2">
          <table
              cellPadding="0"
              cellSpacing="0"
              width="100%"
            >
              <tbody>
                <td style={{
                    padding: '6px 0 2px',
                    fontSize: '0',
                    lineHeight: '14px',
                    textAlign: 'center',
                    verticalAlign: 'top',
                    width: '15px'
                  }}>
                  <img
                    src="https://eleks.com/files/email-signature/address-1.png"
                    alt="ELEKS"
                  />
                </td>
                <td
                  dangerouslySetInnerHTML={{__html: props.selectedCity.address}}
                  style={{
                    color: '#000',
                    fontFamily: 'Arial',
                    fontSize: '13px',
                    lineHeight: '16px',
                    padding: '4px 0 2px 7px',
                    verticalAlign: 'top'
                  }}
                >
                </td>
              </tbody>
          </table>
        </td>
      </tr>))}
    </tbody>
  </table>
);

ResultTable.propTypes = {
  id: PropTypes.string.isRequired,
  fullName: PropTypes.string.isRequired,
  position: PropTypes.string.isRequired,
  mobilePhone: PropTypes.string.isRequired,
  landlinePhone: PropTypes.string.isRequired,
  skype: PropTypes.string.isRequired,
  selectedCity: PropTypes.object.isRequired
}

export default ResultTable;