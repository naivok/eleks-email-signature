import React, {PropTypes} from 'react';

const FormElement = (props) => (
  <div className="form__element">
    <input
      className={`form__control ${props.inputValue ? 'form__control--filled' : ''}`}
      id={props.inputName}
      name={props.inputName}
      onChange={props.inputOnChange}
      type="text"
      value={props.inputValue}
    />
    <label
      className="form__label"
      htmlFor={props.inputName}
    >{props.labelText}{props.inputRequired && <sup className="form__label__required">*</sup>}</label>
    {props.inputIcon && (<span className="form__icon" dangerouslySetInnerHTML={{__html: props.inputIcon}}></span>)}
  </div>
);

FormElement.propTypes = {
  inputName: PropTypes.string.isRequired,
  inputValue: PropTypes.string.isRequired,
  inputIcon: PropTypes.string.isRequired,
  inputOnChange: PropTypes.func.isRequired,
  labelText: PropTypes.string.isRequired,
  inputRequired: PropTypes.bool.isRequired
}

export default FormElement;